package ru.psu.movs.trrp.socketmq.api;

public class SubscribeRequest implements Request {
    private static final long serialVersionUID = -1127535195911952961L;
    public final String resourceUrl;
    public final String queueName;

    public SubscribeRequest(String resourceUrl, String queueName) {
        this.resourceUrl = resourceUrl;
        this.queueName = queueName;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.Subscribe;
    }
}
