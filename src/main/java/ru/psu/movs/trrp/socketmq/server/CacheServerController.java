package ru.psu.movs.trrp.socketmq.server;

import ru.psu.movs.trrp.socketmq.AppConfig;
import ru.psu.movs.trrp.socketmq.api.ListResourcesResponse;
import ru.psu.movs.trrp.socketmq.api.Request;
import ru.psu.movs.trrp.socketmq.api.SubscribeRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CacheServerController implements Runnable {
    private final CacheServerService service;
    private final AppConfig appConfig;

    public CacheServerController(CacheServerService service) {
        appConfig = AppConfig.load();
        this.service = service;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(
                appConfig.cacheServer.port, 50, InetAddress.getByName(appConfig.cacheServer.host))) {
            System.out.printf("[CacheServerController] Running at %s:%d%n", appConfig.cacheServer.host, appConfig.cacheServer.port);
            ExecutorService pool = Executors.newCachedThreadPool();
            try {
                while (true) {
                    Socket socket = serverSocket.accept();
                    pool.execute(() -> handleConnection(socket));
                }
            } finally {
                pool.shutdown();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleConnection(Socket socket) {
        try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {
            Request objFromStream = (Request) ois.readObject();
            switch (objFromStream.getRequestType()) {
                case ListResources:
                    Set<String> availableResources = service.listResources();
                    try (ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
                        oos.writeObject(new ListResourcesResponse(availableResources));
                        oos.flush();
                    }
                    break;
                case Subscribe:
                    SubscribeRequest subscribeRequest = (SubscribeRequest) objFromStream;
                    service.subscribe(subscribeRequest.resourceUrl, subscribeRequest.queueName);
                    break;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
