package ru.psu.movs.trrp.socketmq.client;

import com.rabbitmq.client.*;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import org.apache.commons.lang3.SerializationUtils;
import ru.psu.movs.trrp.socketmq.AppConfig;
import ru.psu.movs.trrp.socketmq.api.ListResourcesRequest;
import ru.psu.movs.trrp.socketmq.api.ListResourcesResponse;
import ru.psu.movs.trrp.socketmq.api.SubscribeRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public class Client implements Runnable {
    private final Channel mqChannel;
    private final AppConfig appConfig;

    public Client() throws IOException, TimeoutException {
        appConfig = AppConfig.load();
        AppConfig.MessageQueueServer messageQueueServer = appConfig.messageQueueServer;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(messageQueueServer.host);
        factory.setPort(messageQueueServer.port);
        factory.setUsername(messageQueueServer.username);
        factory.setPassword(messageQueueServer.password);
        mqChannel = factory.newConnection().createChannel();
    }

    public static void main(String[] args) throws IOException, TimeoutException {
        new Client().run();
    }

    @Override
    public void run() {
        System.out.println("[Client] Running");
        Set<String> resources = getResources();
        if (resources != null)
            resources.forEach(this::subscribeTo);
    }

    private Set<String> getResources() {
        try (Socket socket = new Socket(appConfig.cacheServer.host, appConfig.cacheServer.port);
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
            oos.writeObject(new ListResourcesRequest());
            oos.flush();
            try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {
                ListResourcesResponse receivedObject = (ListResourcesResponse) ois.readObject();
                return receivedObject.availableResources;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void subscribeTo(String resource) {
        try {
            String queueName = mqChannel.queueDeclare("", false, false, true, null).getQueue();
            System.out.println(queueName);
            Consumer consumer = new DefaultConsumer(mqChannel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    SyndEntryImpl rssEntry = SerializationUtils.deserialize(body);
                    System.out.printf("'%s' @ %s%n", rssEntry.getTitle(), rssEntry.getUri());
                }
            };
            mqChannel.basicConsume(queueName, true, consumer);
            try (Socket socket = new Socket(appConfig.cacheServer.host, appConfig.cacheServer.port);
                 ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
                oos.writeObject(new SubscribeRequest(resource, queueName));
                oos.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
