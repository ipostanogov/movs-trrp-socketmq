package ru.psu.movs.trrp.socketmq.api;

import java.io.Serializable;
import java.util.Set;

public class ListResourcesResponse implements Serializable {
    private static final long serialVersionUID = 5603394495217212145L;
    public final Set<String> availableResources;

    public ListResourcesResponse(Set<String> availableResources) {
        this.availableResources = availableResources;
    }
}
